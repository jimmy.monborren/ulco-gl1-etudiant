#include "Bottle.hpp"

#include <sstream>

double alcoholVolume(const Bottle & b) {
    return (b._vol * b._deg) / 100;
}

double alcoholVolume(const std::vector<Bottle> & bs) {
    double vol = 0;

    for(int i = 0; i<bs.size(); i++) {
        vol += alcoholVolume(bs[i]);
    }

    return vol;
}

bool isPoivrot(const std::vector<Bottle> & bs) {
    return alcoholVolume(bs) >= 0.1;
}

std::vector<Bottle> readBottles(std::istream & is) {
    std::vector<Bottle> bottles;
    std::string string;
    Bottle b = Bottle();

    while(is) {
        string = is.get();

        auto start = 0U;
        auto end = string.find(";");
        int i = 0;
        while(end != std::string::npos) {
            std::string valeur = string.substr(start, end - start);
            start = end + 1;
            end = string.find(";", start);
            i++;
            std::cout << valeur << std::endl;
            switch (i)
            {
                case 0:
                    b._name = valeur;
                    break;
                case 1:
                    b._vol = std::stoi(valeur);
                    break;
                case 2:
                    b._deg = std::stoi(valeur);
                    break;
                default:
                    break;
            }
        }

        bottles.push_back(b);
    }

    return bottles;
}

