#include "../src/Bottle.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init Chimay" ) {
    const Bottle b {"Chimay", 0.75, 0.08};
    REQUIRE(b._name == "Chimay");
    REQUIRE(b._vol == 0.75);
    REQUIRE(b._deg == 0.08);
}

TEST_CASE( "init Orange Pur Jus" ) {
    const Bottle b {"Orange Pur Jus", 1, 0};
    REQUIRE(b._name == "Orange Pur Jus");
    REQUIRE(b._vol == 1);
    REQUIRE(b._deg == 0);
}

TEST_CASE( "alcoholVolume Chimay" ) {
    Bottle b {"Chimey", 0.75, 0.08};
    REQUIRE(alcoholVolume(b) == 0.06);
}

TEST_CASE( "alcoholVolume Orange Pur Jus" ) {
    Bottle b {"Orange Pur Jus", 1, 0};
    REQUIRE(alcoholVolume(b) == 0);
}

TEST_CASE( "alcoholVolume Chimay + Orange" ) {
    std::vector<Bottle> b;
    Bottle bs {"Orange Pur Jus", 1, 0};
    Bottle bs2 {"Chimey", 0.75, 0.08};
    b.push_back(bs);
    b.push_back(bs2);

    REQUIRE(alcoholVolume(b) == 0.06);
}

TEST_CASE( "alcoholVolume Chimay + Chimay" ) {
    std::vector<Bottle> b;
    Bottle bs {"Chimey", 0.75, 0.08};
    Bottle bs2 {"Chimey", 0.75, 0.08};
    b.push_back(bs);
    b.push_back(bs2);

    REQUIRE(alcoholVolume(b) == 0.12);
}

TEST_CASE( "isPoivrot Chimay + Orange" ) {
    std::vector<Bottle> b;
    Bottle bs {"Orange Pur Jus", 1, 0};
    Bottle bs2 {"Chimey", 0.75, 0.08};
    b.push_back(bs);
    b.push_back(bs2);

    REQUIRE(!isPoivrot(b));
}

TEST_CASE( "isPoivrot Chimay + Chimay" ) {
    std::vector<Bottle> b;
    Bottle bs {"Chimey", 0.75, 0.08};
    Bottle bs2 {"Chimey", 0.75, 0.08};
    b.push_back(bs);
    b.push_back(bs2);

    REQUIRE(isPoivrot(b));
}

TEST_CASE( "readBottles Chimay" ) {

}

TEST_CASE( "readBottles Chimay + Orange" ) {
    // TODO
}

