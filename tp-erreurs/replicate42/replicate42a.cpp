#include <iostream>
#include <exception>
#include <vector>

enum status_t { OK, TOO_LOW, TOO_HIGH, IS_37, IS_NEGATIVE };

status_t STATUS;

enum NumError { Is37, IsNegative, TooLow, TooHigh, OK };

// Reads a number in stdin and sets STATUS (to OK, IS_37 or IS_NEGATIVE).
int readPositiveButNot37()
{
    std::string line;
    std::cout << "enter num: ";
    std::flush(std::cout);
    std::getline(std::cin, line);
    int num = stod(line);

    if (num < 0)
        throw NumError::IsNegative;
    if (num == 37)
        throw NumError::Is37;
    return num;
}

int main()
{
    try
    {
        // read number
        const int num = readPositiveButNot37();
        std::cout << "num = " << num << std::endl;

        // build vector
        const auto v = replicate42(num);

        // print vector
        for (const int x : v)
            std::cout << x << " ";
        std::cout << std::endl;
    }
    catch (NumError error)
    {
        switch (error)
        {
        case NumError::Is37:
            std::cerr << "error: 37" << std::endl;
            break;
        case NumError::IsNegative:
            std::cerr << "error: negative" << std::endl;
            break;
        case NumError::TooLow:
            std::cerr << "error: too low" << std::endl;
            break;
        case NumError::TooHigh:
            std::cerr << "error: too high" << std::endl;
            break;
        }
    }
    return 0;
}

