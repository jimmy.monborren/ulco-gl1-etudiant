#include "Game.hpp"
#include <iostream>

int main() {
    Game * game = new Game();
    bool haswon = false;

    while(!haswon && game->canPlay()) {
        game->afficher();
        std::string line;
        if (not std::getline(std::cin, line))
            throw std::invalid_argument("bad input");
        try {
            unsigned int number = std::stoi(line);
            game->ajouterHistorique(line);
            haswon = game->tryNumber(number);
        }
        catch (int c) {
            std::cout << "parse error: " << c << std::endl;
        }
    }

    if(haswon) 
        std::cout << "You won!" << std::endl;
    else
    {
        std::cout << "You Loose!" << std::endl;
        game->showNumber();
    } 

    return 0;
}

