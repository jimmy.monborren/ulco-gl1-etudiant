#include "Game.hpp"


Game::Game() {
    std::random_device r;

    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(1, 100);
    number = uniform_dist(e1);
    tries = 5;
    history = "history:";
}

void Game::ajouterHistorique(std::string line) {
    history += " " + line;
}

void Game::afficher() {
    std::cout << history << std::endl;
    std::cout << "enter a number (1 - 100): ";
    std::cout.flush();
}

bool Game::tryNumber(unsigned int number) {
    if(number > 100) {
        std::cout << "Invalid!" << std::endl;
        return false;
    }

    if(number < 1) {
        std::cout << "Invalid!" << std::endl;
        return false;
    }

    if(this->number < number) {
        std::cout << "Too Big!" << std::endl;
        tries--;
        return false;
    }

    if(this->number > number) {
        std::cout << "Too Low!"  << std::endl;
        tries--;
        return false;
    }

    

    return true;
}

bool Game::canPlay() {
    return tries > 0;
}

void Game::showNumber() {
    std::cout << "target: "  << this->number << std::endl;
}