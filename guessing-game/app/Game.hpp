#ifndef GAME_HPP
#define GAME_HPP

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <random>

class Game {
private:
    unsigned int number;
    unsigned int tries;
    std::string history;
public:
    Game();
    bool tryNumber(unsigned int number);
    bool canPlay();
    void showNumber();
    void afficher();
    void ajouterHistorique(std::string line);
};

#endif