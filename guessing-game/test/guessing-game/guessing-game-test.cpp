#include <app/Game.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "guessing-game try 42 " ) {
    Game* game = new Game();

    REQUIRE(game->tryNumber(42));
}

TEST_CASE( "guessing-game try 85 " ) {
    Game* game = new Game();

    REQUIRE(game->tryNumber(85));
}

TEST_CASE( "guessing-game try 81" ) {
    Game* game = new Game();

    REQUIRE(game->tryNumber(81));
}

